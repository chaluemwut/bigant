#!/usr/bin/env python
"""mapper.py"""
import sys, re, gensim
from gensim import corpora, models, similarities,utils
import numpy as np
import pickle


for line in sys.stdin:
    words = line.split(' ')
    # increase counters
    number_of_ant = words[0]
    number_of_alha = words[1]
    number_of_eta = words[2]