import plotly.io as pio
import plotly.plotly as py
import plotly.graph_objs as go
import os
import numpy as np


# N = 100
# x = np.random.rand(N)
# y = np.random.rand(N)
# colors = np.random.rand(N)
# sz = np.random.rand(N)*30
#
# fig = go.Figure()
# fig.add_scatter(x=x,
#                 y=y,
#                 mode='markers',
#                 showlegend=True,
#                 marker={'size': sz,
#                         'color': colors,
#                         'opacity': 0.6,
#                         'colorscale': 'Viridis'
#                        });
#
# pio.write_image(fig, 'images/fig1.png')
# py.iplot(fig, filename = "3d annotations")

data = [
"Topic:: 0 0.011*network + 0.008*model + 0.007*neuron + 0.007*neural + 0.005*input + 0.005*information + 0.005*function + 0.005*region + 0.005*frequency + 0.004*set",
"Topic:: 1 0.025*network + 0.013*input + 0.010*weight + 0.009*neural + 0.009*unit + 0.009*output + 0.008*learning + 0.007*algorithm + 0.007*function + 0.006*system",
"Topic:: 2 0.016*model + 0.010*cell + 0.008*learning + 0.006*data + 0.006*system + 0.005*neuron + 0.005*function + 0.005*network + 0.004*circuit + 0.004*algorithm"
]

from random import randint

import plotly.plotly as py
import plotly.graph_objs as go

def plot():
    colors = np.random.rand(100)
    sz = np.random.rand(100) * 30
    fig = go.Figure()
    x = []
    y = []
    for l in data:
        l = l.replace('Topic:: ', '')
        topic_id = l[:2]
        line_data = l[2:]
        word_data = line_data.split(' + ')
        max_plot = 10*len(word_data)
        for l_word_data in word_data:
            x.append(randint(0, max_plot))
            y.append(randint(0, max_plot))
            word_list = l_word_data.split('*')

    fig.add_scatter(x=x, y=y, mode='markers', showlegend=False, marker={
        'size': 10,
        'color': colors,
        'size': sz,
        'colorscale': 'Viridis'
    })
    pio.write_image(fig, 'images/fig1.png')

if __name__ == '__main__':
    plot()


