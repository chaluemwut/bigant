import plotly
import plotly.plotly as py
import plotly.graph_objs as go
from random import randint

import plotly.plotly as py
import plotly.graph_objs as go

import numpy as np

plotly.tools.set_credentials_file('chaluemwut@hotmail.com', 'R2f2yPMTbx4lzbx5DGUF')

data2 = [
    "Topic:: 0 0.011*network + 0.008*model + 0.007*neuron + 0.007*neural + 0.005*input + 0.005*information + 0.005*function + 0.005*region + 0.005*frequency + 0.004*set",
    "Topic:: 1 0.025*network + 0.013*input + 0.010*weight + 0.009*neural + 0.009*unit + 0.009*output + 0.008*learning + 0.007*algorithm + 0.007*function + 0.006*system",
    "Topic:: 2 0.016*model + 0.010*cell + 0.008*learning + 0.006*data + 0.006*system + 0.005*neuron + 0.005*function + 0.005*network + 0.004*circuit + 0.004*algorithm",
    "Topic:: 3 0.016*learning + 0.015*network + 0.008*function + 0.008*algorithm + 0.008*weight + 0.007*set + 0.006*problem + 0.006*model + 0.005*input + 0.005*system",
    "Topic:: 4 0.020*network + 0.010*model + 0.010*input + 0.009*neuron + 0.007*system + 0.007*cell + 0.006*function + 0.005*neural + 0.005*phase + 0.005*unit",
    "Topic:: 5 0.015*network + 0.010*model + 0.010*learning + 0.009*error + 0.008*set + 0.007*neural + 0.006*training + 0.006*method + 0.006*function + 0.005*algorithm",
    "Topic:: 6 0.015*model + 0.013*network + 0.008*learning + 0.007*function + 0.007*neural + 0.007*data + 0.007*input + 0.006*system + 0.006*algorithm + 0.006*neuron",
    "Topic:: 7 0.018*network + 0.013*set + 0.010*training + 0.009*learning + 0.007*neural + 0.006*input + 0.006*function + 0.006*point + 0.006*pattern + 0.006*data",
    "Topic:: 8 0.013*model + 0.011*network + 0.009*input + 0.008*data + 0.008*object + 0.007*system + 0.006*image + 0.006*output + 0.006*neural + 0.006*speech",
    "Topic:: 9 0.012*model + 0.010*network + 0.009*learning + 0.009*unit + 0.008*input + 0.008*algorithm + 0.006*function + 0.006*pattern + 0.005*vector + 0.005*weight",
    "Topic:: 10 0.015*model + 0.010*network + 0.009*unit + 0.008*data + 0.007*input + 0.006*learning + 0.006*signal + 0.005*information + 0.005*pattern + 0.005*system",
    "Topic:: 11 0.015*network + 0.010*model + 0.010*input + 0.010*neural + 0.009*system + 0.008*function + 0.007*training + 0.007*data + 0.007*set + 0.006*learning",
    "Topic:: 12 0.009*network + 0.008*algorithm + 0.008*vector + 0.007*set + 0.007*cell + 0.007*input + 0.006*model + 0.006*training + 0.005*neural + 0.005*data",
    "Topic:: 13 0.016*function + 0.014*network + 0.012*learning + 0.008*input + 0.008*set + 0.007*algorithm + 0.007*training + 0.006*system + 0.006*problem + 0.006*error",
    "Topic:: 14 0.012*function + 0.008*learning + 0.008*model + 0.007*point + 0.007*system + 0.006*network + 0.006*neural + 0.006*cell + 0.006*algorithm + 0.005*unit",
    "Topic:: 15 0.016*network + 0.010*system + 0.008*function + 0.007*learning + 0.007*set + 0.007*unit + 0.007*input + 0.007*neural + 0.006*neuron + 0.006*control",
    "Topic:: 16 0.012*learning + 0.007*data + 0.007*algorithm + 0.006*system + 0.006*problem + 0.006*information + 0.006*function + 0.005*number + 0.005*set + 0.005*unit",
    "Topic:: 17 0.013*learning + 0.012*function + 0.010*model + 0.010*network + 0.008*data + 0.005*system + 0.005*problem + 0.005*error + 0.005*input + 0.005*number",
    "Topic:: 18 0.011*model + 0.009*network + 0.009*data + 0.007*neural + 0.007*function + 0.006*training + 0.006*input + 0.006*algorithm + 0.006*error + 0.006*learning",
    "Topic:: 19 0.011*network + 0.010*set + 0.009*model + 0.009*training + 0.009*algorithm + 0.008*function + 0.008*neural + 0.007*data + 0.007*problem + 0.006*point"
]

duplicate_word_list = []

class WordData(object):
    word = None
    x = None
    y = None


def is_duplicate(word_data):
    for w in duplicate_word_list:
        if w.word == word_data.word:
            return True, w

    return False, None

def plot():
    colors = np.random.rand(100)
    sz = np.random.rand(100) * 60

    pos_x_word_list = []
    pos_y_word_list = []
    text = []

    x_topic = []
    y_topic = []
    text_topic = []
    line_center_plot = []



    for l in data2:
        topic = l.split(' ')
        l = l.replace('Topic:: ', '')
        line_data = l[2:]
        word_data = line_data.split(' + ')
        max_plot = 100 * len(word_data)
        x_topic_data = randint(0, max_plot)
        y_topic_data = randint(0, max_plot)
        x_topic.append(x_topic_data)
        y_topic.append(y_topic_data)
        text_topic.append(topic[0]+topic[1])

        for l_word_data in word_data:
            word_list = l_word_data.split('*')
            word = word_list[1]
            pos_x_word = randint(0, max_plot)
            pos_y_word = randint(0, max_plot)

            word_data = WordData()
            word_data.word = word
            word_data.x = pos_x_word
            word_data.y = pos_y_word

            dup_flag, dup_word_data = is_duplicate(word_data)

            text.append(word_list[1])


            pos_x_word_list.append(pos_x_word)
            pos_y_word_list.append(pos_y_word)
            line_center = go.Scatter(
                x=[x_topic_data, pos_x_word],
                y=[y_topic_data, pos_y_word],
                mode='lines',
                opacity=0.1
            )
            line_center_plot.append(line_center)

    data_topic = go.Scatter(
        x=x_topic,
        y=y_topic,
        mode='markers+text',
        textposition='middle center',
        text=text_topic,
        marker={
            'symbol': 'square',
            'size': 30
        }
    )

    data_word = go.Scatter(
        x=pos_x_word_list,
        y=pos_y_word_list,
        mode='markers+text',
        textposition='middle center',
        text=text,
        marker={
            'symbol': "circle",
            'color': colors,
            'size': sz,
            'colorscale': 'Viridis'
        }
    )

    data = []
    for line_center in line_center_plot:
        data.append(line_center)

    data.append(data_topic)
    data.append(data_word)

    py.plot(data, filename='basic-scatter')


if __name__ == '__main__':
    plot()
